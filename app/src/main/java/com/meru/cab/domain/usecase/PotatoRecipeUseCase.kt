package com.meru.cab.domain.usecase

import com.meru.cab.domain.entities.Recipe
import com.meru.cab.domain.repository.DomainRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PotatoRecipeUseCase @Inject constructor(val repository: DomainRepo) {

    fun getPotatoRecipes(): Flow<ArrayList<Recipe>?> {
        return repository.getPotatoRecipes()
    }

    fun deletePotato(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.deletePotatoRecipe(recipe)
    }

    fun likeDislikePotato(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.likeDislikePotato(recipe)
    }


}