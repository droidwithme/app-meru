package com.meru.cab.domain.usecase

import com.meru.cab.domain.entities.Recipe
import com.meru.cab.domain.repository.DomainRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ChickenRecipeUseCase @Inject constructor(val repository: DomainRepo) {

    fun getChickenRecipes(): Flow<ArrayList<Recipe>?> {
        return repository.getChickenRecipes()
    }

    fun deleteChicken(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.deleteChickenRecipe(recipe)
    }

    fun likeDislikeChicken(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.likeDislikeChicken(recipe)
    }


}