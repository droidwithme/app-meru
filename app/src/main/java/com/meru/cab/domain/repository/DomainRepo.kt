package com.meru.cab.domain.repository

import com.meru.cab.domain.entities.*
import kotlinx.coroutines.flow.Flow

interface DomainRepo {
    //for potato usecase
    fun getPotatoRecipes(): Flow<ArrayList<Recipe>?>
    fun deletePotatoRecipe(recipe: Recipe):Flow<ArrayList<Recipe>?>
    fun likeDislikePotato(recipe: Recipe): Flow<ArrayList<Recipe>?>

    //for chicken usecase
    fun getChickenRecipes(): Flow<ArrayList<Recipe>?>
    fun deleteChickenRecipe(recipe: Recipe): Flow<ArrayList<Recipe>?>
    fun likeDislikeChicken(recipe: Recipe): Flow<ArrayList<Recipe>?>

    //for fish usecase
    fun getFishRecipes(): Flow<ArrayList<Recipe>?>
    fun deleteFishRecipe(recipe: Recipe): Flow<ArrayList<Recipe>?>
    fun likeDislikeFish(recipe: Recipe): Flow<ArrayList<Recipe>?>

}