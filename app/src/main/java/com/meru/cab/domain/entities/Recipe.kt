package com.meru.cab.domain.entities

import com.meru.cab.data.api.database.entity.ChickenRecipeEntity
import com.meru.cab.data.api.database.entity.FishRecipeEntity
import com.meru.cab.data.api.database.entity.PotatoRecipeEntity

data class Recipe(
    var _id: String,
    var image_url: String,
    var publisher: String,
    var publisher_url: String,
    var recipe_id: String,
    var social_rank: Double,
    var source_url: String,
    var title: String,
    var isLiked: Boolean
) {
    companion object {
        fun toPotato(recipe: Recipe): PotatoRecipeEntity {
            return PotatoRecipeEntity(
                recipe._id,
                recipe.image_url,
                recipe.publisher,
                recipe.publisher_url,
                recipe.recipe_id,
                recipe.social_rank,
                recipe.source_url,
                recipe.title,
                recipe.isLiked
            )
        }
        fun toChicken(recipe: Recipe): ChickenRecipeEntity {
            return ChickenRecipeEntity(
                recipe._id,
                recipe.image_url,
                recipe.publisher,
                recipe.publisher_url,
                recipe.recipe_id,
                recipe.social_rank,
                recipe.source_url,
                recipe.title,
                recipe.isLiked
            )
        }

        fun toFish(recipe: Recipe): FishRecipeEntity {
            return FishRecipeEntity(
                recipe._id,
                recipe.image_url,
                recipe.publisher,
                recipe.publisher_url,
                recipe.recipe_id,
                recipe.social_rank,
                recipe.source_url,
                recipe.title,
                recipe.isLiked
            )
        }
    }
}