package com.meru.cab.domain.usecase

import com.meru.cab.domain.entities.Recipe
import com.meru.cab.domain.repository.DomainRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FishRecipeUseCase @Inject constructor(val repository: DomainRepo) {

    fun getFishRecipes(): Flow<ArrayList<Recipe>?> {
        return repository.getFishRecipes()
    }

    fun deleteFish(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.deleteFishRecipe(recipe)
    }

    fun likeDislikeFish(recipe: Recipe): Flow<ArrayList<Recipe>?> {
        return repository.likeDislikeFish(recipe)
    }


}