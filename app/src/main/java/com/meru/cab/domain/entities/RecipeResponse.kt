package com.meru.cab.domain.entities

data class RecipeResponse(
    var count: Int,
    var recipes: List<Recipe>
)