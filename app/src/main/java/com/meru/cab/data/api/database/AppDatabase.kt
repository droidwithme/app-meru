package com.meru.cab.data.api.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.meru.cab.data.api.database.entity.ChickenRecipeEntity
import com.meru.cab.data.api.database.entity.FishRecipeEntity
import com.meru.cab.data.api.database.entity.PotatoRecipeEntity

@Database(
    entities = [PotatoRecipeEntity::class, FishRecipeEntity::class, ChickenRecipeEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun potatoRecipeDao(): PotatoRecipeDao
    abstract fun chickenRecipeDao(): ChickenRecipeDao
    abstract fun fishRecipeDao(): FishRecipeDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "recipe-list.db"
        )
            .build()
    }
}