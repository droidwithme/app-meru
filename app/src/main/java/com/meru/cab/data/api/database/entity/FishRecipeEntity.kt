package com.meru.cab.data.api.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "fish")
data class FishRecipeEntity(
    @PrimaryKey
    var _id: String,
    var image_url: String,
    var publisher: String,
    var publisher_url: String,
    var recipe_id: String,
    var social_rank: Double,
    var source_url: String,
    var title: String,
    var isLiked: Boolean
)