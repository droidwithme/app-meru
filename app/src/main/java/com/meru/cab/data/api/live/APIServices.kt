package com.meru.cab.data.api.live

import com.meru.cab.domain.entities.RecipeResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton


@Singleton
interface APIServices {

    companion object {
        const val GET_RECIPE: String = ("search")

    }

    @GET(GET_RECIPE)
    suspend fun getRecipe(@Query("q") query: String): Response<RecipeResponse>

}