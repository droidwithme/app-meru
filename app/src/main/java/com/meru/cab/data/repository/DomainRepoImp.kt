package com.meru.cab.data.repository

import android.content.Context
import androidx.room.Room
import com.meru.cab.data.api.database.AppDatabase
import com.meru.cab.data.api.database.entity.PotatoRecipeEntity
import com.meru.cab.data.api.live.APIServices
import com.meru.cab.domain.entities.Recipe
import com.meru.cab.domain.entities.RecipeResponse
import com.meru.cab.domain.repository.DomainRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

import android.net.ConnectivityManager
import android.util.Log
import com.meru.cab.data.api.database.entity.ChickenRecipeEntity
import com.meru.cab.data.api.database.entity.FishRecipeEntity


@Singleton
class DomainRepoImp @Inject constructor(
    private val APIServices: APIServices,
    private val context: Context
) : BaseRepoImp(), DomainRepo {
    var db: AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "recipe-list.db").build()

    //for potato
    override fun getPotatoRecipes(): Flow<ArrayList<Recipe>?> = flow {
        Log.e("DomainRepoImp", "getRecipe()")

        if (isNetworkAvailable(context)) {
            emit(getPotatoRecipeFromServer())
        } else {
            emit(getPotatoRecipeList())
        }
    }

    override fun deletePotatoRecipe(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.potatoRecipeDao().delete(Recipe.toPotato(recipe))
        emit(getPotatoRecipeList())
    }

    override fun likeDislikePotato(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.potatoRecipeDao().updateRecipe(Recipe.toPotato(recipe))
        emit(getPotatoRecipeList())
    }

    private suspend fun getPotatoRecipeFromServer(): ArrayList<Recipe> {
        val data = safeApiCall(
            { APIServices.getRecipe("Potato") },
            "getAllRecipeFromServer"
        ) as RecipeResponse
        savePotatoToDatabase(data.recipes)
        return getPotatoRecipeList()
    }

    private fun savePotatoToDatabase(recipeList: List<Recipe>) { 
        val mappedToRecipeEntity = recipeList.map {
            PotatoRecipeEntity(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                false
            )
        }

        if (mappedToRecipeEntity.isNotEmpty()) {
            db.potatoRecipeDao().insertAll(mappedToRecipeEntity)
        }
    }

    private fun getPotatoRecipeList(): ArrayList<Recipe> {
        val potatoRecipeEntityList: ArrayList<PotatoRecipeEntity> =
            db.potatoRecipeDao().getAll() as ArrayList<PotatoRecipeEntity>

        return potatoRecipeEntityList.map {
            Recipe(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                it.isLiked
            )
        } as ArrayList<Recipe>
    }


    //for chicken
    override fun getChickenRecipes(): Flow<ArrayList<Recipe>?> = flow {
        Log.e("DomainRepoImp", "getChickenRecipes()")

        if (isNetworkAvailable(context)) {
            emit(getChickenRecipeFromServer())
        } else {
            emit(getChickenRecipeList())
        }
    }

    override fun deleteChickenRecipe(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.chickenRecipeDao().delete(Recipe.toChicken(recipe))
        emit(getChickenRecipeList())
    }

    override fun likeDislikeChicken(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.chickenRecipeDao().updateRecipe(Recipe.toChicken(recipe))
        emit(getChickenRecipeList())
    }

    private suspend fun getChickenRecipeFromServer(): ArrayList<Recipe> {
        val data = safeApiCall(
            { APIServices.getRecipe("Chicken") },
            "getAllRecipeFromServer"
        ) as RecipeResponse
        saveChickenToDatabase(data.recipes)
        return getChickenRecipeList()
    }

    private fun saveChickenToDatabase(recipeList: List<Recipe>) {
        val mappedToRecipeEntity = recipeList.map {
            ChickenRecipeEntity(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                false
            )
        }

        if (mappedToRecipeEntity.isNotEmpty()) {
            db.chickenRecipeDao().insertAll(mappedToRecipeEntity)
        }
    }

    private fun getChickenRecipeList(): ArrayList<Recipe> {
        val chickenRecipeEntityList: ArrayList<ChickenRecipeEntity> =
            db.chickenRecipeDao().getAll() as ArrayList<ChickenRecipeEntity>
        return chickenRecipeEntityList.map {
            Recipe(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                it.isLiked
            )
        } as ArrayList<Recipe>
    }


    //for fish
    override fun getFishRecipes(): Flow<ArrayList<Recipe>?> = flow {
        Log.e("DomainRepoImp", "getFishRecipes()")

        if (isNetworkAvailable(context)) {
            emit(getFishRecipeFromServer())
        } else {
            emit(getFishRecipeList())
        }
    }

    override fun deleteFishRecipe(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.fishRecipeDao().delete(Recipe.toFish(recipe))
        emit(getFishRecipeList())
    }

    override fun likeDislikeFish(recipe: Recipe): Flow<ArrayList<Recipe>?> = flow {
        db.fishRecipeDao().updateRecipe(Recipe.toFish(recipe))
        emit(getFishRecipeList())
    }

    private suspend fun getFishRecipeFromServer(): ArrayList<Recipe> {
        val data = safeApiCall(
            { APIServices.getRecipe("Fish") },
            "getAllRecipeFromServer"
        ) as RecipeResponse
        saveFishToDatabase(data.recipes)
        return getFishRecipeList()
    }

    private fun saveFishToDatabase(recipeList: List<Recipe>) {
        val mappedToRecipeEntity = recipeList.map {
            FishRecipeEntity(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                false
            )
        }

        if (mappedToRecipeEntity.isNotEmpty()) {
            db.fishRecipeDao().insertAll(mappedToRecipeEntity)
        }
    }

    private fun getFishRecipeList(): ArrayList<Recipe> {
        val potatoRecipeEntityList: ArrayList<FishRecipeEntity> =
            db.fishRecipeDao().getAll() as ArrayList<FishRecipeEntity>
        return potatoRecipeEntityList.map {
            Recipe(
                it._id,
                it.image_url,
                it.publisher,
                it.publisher_url,
                it.recipe_id,
                it.social_rank,
                it.source_url,
                it.title,
                it.isLiked
            )
        } as ArrayList<Recipe>
    }


    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}