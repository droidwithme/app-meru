package com.meru.cab.data.api.database

import androidx.room.*
import com.meru.cab.data.api.database.entity.PotatoRecipeEntity

@Dao
interface PotatoRecipeDao {

    @Query("SELECT * FROM potato")
    fun getAll(): List<PotatoRecipeEntity>

    @Insert
    fun insert(vararg potatoRecipe: PotatoRecipeEntity)

    @Delete
    fun delete(potatoRecipe: PotatoRecipeEntity)

    @Update(entity = PotatoRecipeEntity::class)
    fun updateRecipe(vararg potatoRecipe: PotatoRecipeEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertAll(list: List<PotatoRecipeEntity>)

    @Query("UPDATE potato SET isLiked = :isLiked WHERE _id = :id")
    fun update(id: String, isLiked: Boolean)
}