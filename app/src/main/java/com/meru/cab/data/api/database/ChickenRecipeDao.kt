package com.meru.cab.data.api.database

import androidx.room.*
import com.meru.cab.data.api.database.entity.ChickenRecipeEntity

@Dao
interface ChickenRecipeDao {

    @Query("SELECT * FROM chicken")
    fun getAll(): List<ChickenRecipeEntity>

    @Insert
    fun insert(vararg chickenRecipe: ChickenRecipeEntity)

    @Delete
    fun delete(chickenRecipe: ChickenRecipeEntity)

    @Update(entity = ChickenRecipeEntity::class)
    fun updateRecipe(vararg chickenRecipe: ChickenRecipeEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertAll(list: List<ChickenRecipeEntity>)

    @Query("UPDATE chicken SET isLiked = :isLiked WHERE _id = :id")
    fun update(id: String, isLiked: Boolean)
}