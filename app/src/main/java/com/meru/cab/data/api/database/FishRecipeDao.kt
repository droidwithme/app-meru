package com.meru.cab.data.api.database

import androidx.room.*
import com.meru.cab.data.api.database.entity.FishRecipeEntity

@Dao
interface FishRecipeDao {

    @Query("SELECT * FROM fish")
    fun getAll(): List<FishRecipeEntity>

    @Insert
    fun insert(vararg fishRecipe: FishRecipeEntity)

    @Delete
    fun delete(fishRecipe: FishRecipeEntity)

    @Update(entity = FishRecipeEntity::class)
    fun updateRecipe(vararg fishRecipe: FishRecipeEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertAll(list: List<FishRecipeEntity>)


    @Query("UPDATE fish SET isLiked = :isLiked WHERE _id = :id")
    fun update(id: String, isLiked: Boolean)
}