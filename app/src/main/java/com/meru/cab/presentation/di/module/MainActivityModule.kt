package com.meru.cab.presentation.di.module

import com.meru.cab.data.repository.DomainRepoImp
import com.meru.cab.presentation.main.MainViewModel
import dagger.Module
import dagger.Provides



@Module
class MainActivityModule {

    @Provides
    internal fun provideMainViewModel(lmdRepo: DomainRepoImp): MainViewModel {
        return MainViewModel(lmdRepo)
    }

}