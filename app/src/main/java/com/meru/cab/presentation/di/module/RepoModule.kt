package com.meru.cab.presentation.di.module

import android.content.Context
import com.meru.cab.data.api.live.APIServices
import com.meru.cab.data.repository.DomainRepoImp
import com.meru.cab.domain.repository.DomainRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepoModule {
    @Provides
    @Singleton
    internal fun provideLmdRepository(APIServices: APIServices, context: Context): DomainRepo {
        return DomainRepoImp(APIServices, context)
    }

}