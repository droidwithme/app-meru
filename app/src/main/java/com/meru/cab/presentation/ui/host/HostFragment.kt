package com.meru.cab.presentation.ui.host

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.meru.cab.R
import com.meru.cab.presentation.base.BaseFragment
import com.meru.cab.presentation.main.MainActivity
import com.meru.cab.presentation.ui.potato.PotatoRecipeFragment
import kotlinx.android.synthetic.main.fragment_host.*
import javax.inject.Inject
import androidx.viewpager.widget.ViewPager
import com.meru.cab.presentation.ui.chicken.ChickenRecipeFragment
import com.meru.cab.presentation.ui.fish.FishRecipeFragment


@Suppress("UNCHECKED_CAST")
private val TAG = HostFragment::class.java.simpleName

class HostFragment : BaseFragment<HostFragmentViewModel>() {

    private val SPLASHDISPLAYLENGTH: Long = 3000


    companion object {
        private const val MY_DATA = "data"
        fun newInstance(strData: String?) = HostFragment().apply {
            arguments = bundleOf(
                MY_DATA to strData
            )
        }
    }


    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory

    override fun getLayoutId(): Int = R.layout.fragment_host

    override fun getViewModel(): HostFragmentViewModel =
        ViewModelProviders.of(this, mViewModelFactory).get(HostFragmentViewModel::class.java)

    override fun getLifeCycleOwner(): LifecycleOwner = this
    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PotatoRecipeFragment.newInstance(null), "Potato")
        adapter.addFragment(FishRecipeFragment.newInstance(null), "Fish")
        adapter.addFragment(ChickenRecipeFragment.newInstance(null), "Chicken")
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

    }

    private fun setUp() {
        Log.e(PotatoRecipeFragment.TAG, "setUp()")
        activity as MainActivity
        (activity as MainActivity).showToolBar("Recipes")
    }
}