package com.meru.cab.presentation.di.module.splash

import com.meru.cab.presentation.ui.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SplashFragmentProvider {

    @ContributesAndroidInjector(modules = [(SplashFragmentModule::class)])
    internal abstract fun provideMainFragmentFactory(): SplashFragment


}
