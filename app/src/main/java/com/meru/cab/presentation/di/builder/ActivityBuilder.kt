package com.meru.cab.presentation.di.builder

import com.meru.cab.presentation.di.module.MainActivityModule
import com.meru.cab.presentation.di.module.chicken.ChickenFragmentProvider
import com.meru.cab.presentation.di.module.fish.FishFragmentProvider
import com.meru.cab.presentation.di.module.host.HostFragmentProvider
import com.meru.cab.presentation.di.module.splash.SplashFragmentProvider
import com.meru.cab.presentation.di.module.potato.PotatoFragmentProvider
import com.meru.cab.presentation.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(
        modules = [(MainActivityModule::class), (SplashFragmentProvider::class),
            (PotatoFragmentProvider::class), (FishFragmentProvider::class),
            (ChickenFragmentProvider::class), (HostFragmentProvider::class)]
    )
    internal abstract fun bindMainActivity(): MainActivity

}