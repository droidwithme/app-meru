package com.meru.cab.presentation.di.module.chicken

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.meru.cab.domain.usecase.ChickenRecipeUseCase
import com.meru.cab.domain.usecase.FishRecipeUseCase
import com.meru.cab.presentation.commons.GridSpacingItemDecoration
import com.meru.cab.presentation.commons.ViewModelProviderFactory
import com.meru.cab.presentation.main.MainActivity
import com.meru.cab.presentation.ui.chicken.ChickenRecipeAdapter
import com.meru.cab.presentation.ui.chicken.ChickenRecipeFragment
import com.meru.cab.presentation.ui.chicken.ChickenRecipeFragmentViewModel
import com.meru.cab.presentation.ui.fish.FishRecipeAdapter
import com.meru.cab.presentation.ui.fish.FishRecipeFragment
import com.meru.cab.presentation.ui.fish.FishRecipeFragmentViewModel
import dagger.Module
import dagger.Provides


@Module
class ChickenFragmentModule {


    @Provides
    internal fun provideMainFragmentViewModel(chickenRecipeUseCase: ChickenRecipeUseCase): ChickenRecipeFragmentViewModel {
        return ChickenRecipeFragmentViewModel(chickenRecipeUseCase)
    }

    @Provides
    internal fun mainFragmentViewModelProvider(chickenRecipeFragmentViewModel: ChickenRecipeFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(chickenRecipeFragmentViewModel)
    }

    @Provides
    internal fun provideLinearLayoutManager(fragment: ChickenRecipeFragment): LinearLayoutManager {
        return LinearLayoutManager((fragment.activity as Context?)!!)
    }

    @Provides
    internal fun provideGridSpacingItemDecoration(): GridSpacingItemDecoration {
        return GridSpacingItemDecoration(2, 5, true)
    }

    @Provides
    internal fun provideChickenRecipeAdapter(context: MainActivity): ChickenRecipeAdapter {
        return ChickenRecipeAdapter(ArrayList(), context)
    }

}