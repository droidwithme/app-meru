package com.meru.cab.presentation.ui.splash

import com.meru.cab.presentation.base.BaseViewModel
import com.meru.cab.presentation.base.BaseViewState
import javax.inject.Inject

class SplashFragmentViewModel @Inject constructor() : BaseViewModel<BaseViewState>()