package com.meru.cab.presentation.di.module.host

import com.meru.cab.presentation.ui.host.HostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HostFragmentProvider {

    @ContributesAndroidInjector(modules = [(HostFragmentModule::class)])
    internal abstract fun provideMainFragmentFactory(): HostFragment


}
