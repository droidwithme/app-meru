

package com.meru.cab.presentation.di.module.fish

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.meru.cab.domain.usecase.FishRecipeUseCase
import com.meru.cab.presentation.commons.GridSpacingItemDecoration
import com.meru.cab.presentation.commons.ViewModelProviderFactory
import com.meru.cab.presentation.main.MainActivity
import com.meru.cab.presentation.ui.fish.FishRecipeAdapter
import com.meru.cab.presentation.ui.fish.FishRecipeFragment
import com.meru.cab.presentation.ui.fish.FishRecipeFragmentViewModel
import dagger.Module
import dagger.Provides


@Module
class FishFragmentModule {

    @Provides
    internal fun provideMainFragmentViewModel(fishRecipeUseCase: FishRecipeUseCase): FishRecipeFragmentViewModel {
        return FishRecipeFragmentViewModel(fishRecipeUseCase)
    }

    @Provides
    internal fun mainFragmentViewModelProvider(fishRecipeFragmentViewModel: FishRecipeFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(fishRecipeFragmentViewModel)
   }

    @Provides
    internal fun provideLinearLayoutManager(fragment: FishRecipeFragment): LinearLayoutManager {
        return LinearLayoutManager((fragment.activity as Context?)!!)
    }

    @Provides
    internal fun provideGridSpacingItemDecoration(): GridSpacingItemDecoration {
        return GridSpacingItemDecoration(2, 5, true)
    }

    @Provides
    internal fun provideFishRecipeAdapter(context: MainActivity): FishRecipeAdapter {
        return FishRecipeAdapter(ArrayList(), context)
    }

}