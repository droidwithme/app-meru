package com.meru.cab.presentation.di.module.chicken

import com.meru.cab.presentation.ui.chicken.ChickenRecipeFragment
import com.meru.cab.presentation.ui.fish.FishRecipeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ChickenFragmentProvider {

    @ContributesAndroidInjector(modules = [(ChickenFragmentModule::class)])
    internal abstract fun provideMainFragmentFactory(): ChickenRecipeFragment

}