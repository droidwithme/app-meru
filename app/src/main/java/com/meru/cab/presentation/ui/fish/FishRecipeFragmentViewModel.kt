package com.meru.cab.presentation.ui.fish

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.meru.cab.domain.entities.Recipe
import com.meru.cab.domain.usecase.FishRecipeUseCase
import com.meru.cab.domain.usecase.PotatoRecipeUseCase
import com.meru.cab.presentation.base.BaseViewModel
import com.meru.cab.presentation.base.BaseViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class FishRecipeFragmentViewModel @Inject constructor(private val fishRecipeUseCase: FishRecipeUseCase) :
    BaseViewModel<BaseViewState>() {

    private var TAG = FishRecipeFragmentViewModel::class.java.simpleName


    fun getFishRecipes() {
        Log.e(TAG, "getPotatoRecipes()")
        mUiState.value = BaseViewState.loading(true)
        viewModelScope.launch(Dispatchers.IO) {
            fishRecipeUseCase.getFishRecipes().collect {
                withContext(Dispatchers.Main) { updateUI(it) }
            }
        }
    }

    fun deleteRecipe(recipe: Recipe) {
        Log.e(TAG, "deleteRecipe()")
        mUiState.value = BaseViewState.loading(true)
        viewModelScope.launch(Dispatchers.IO) {
            fishRecipeUseCase.deleteFish(recipe).collect {
                withContext(Dispatchers.Main) { updateUI(it) }
            }
        }
    }

    fun likeDislike(recipe: Recipe) {
        Log.e(TAG, "likeDislike()")
        mUiState.value = BaseViewState.loading(true)
        viewModelScope.launch(Dispatchers.IO) {
            fishRecipeUseCase.likeDislikeFish(recipe).collect {
                withContext(Dispatchers.Main) { updateUI(it) }
            }
        }
    }


    private fun updateUI(it: ArrayList<Recipe>?) {
        Log.e(TAG, "updateUI($it)")
        if (it != null)
            mUiState.postValue(BaseViewState.hasData(it))
        else
            mUiState.postValue(BaseViewState.errorText("no "))
    }

    private fun updateUI(it: String?) {
        Log.e(TAG, "updateUI($it)")
        if (it != null)
            mUiState.postValue(BaseViewState.hasData(it))
        else
            mUiState.postValue(BaseViewState.errorText("no "))
    }

}