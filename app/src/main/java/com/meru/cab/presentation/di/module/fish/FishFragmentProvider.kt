

package com.meru.cab.presentation.di.module.fish

import com.meru.cab.presentation.ui.fish.FishRecipeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FishFragmentProvider {

    @ContributesAndroidInjector(modules =[(FishFragmentModule::class)])
    internal abstract fun provideMainFragmentFactory(): FishRecipeFragment

}