package com.meru.cab.presentation.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.meru.cab.R
import com.meru.cab.presentation.base.BaseFragment
import com.meru.cab.presentation.main.MainActivity
import com.meru.cab.presentation.ui.host.HostFragment
import com.meru.cab.presentation.ui.potato.PotatoRecipeFragment
import javax.inject.Inject


/*
* To list the topics
* */
@Suppress("UNCHECKED_CAST")
private val TAG = SplashFragment::class.java.simpleName

class SplashFragment : BaseFragment<SplashFragmentViewModel>() {

    private val SPLASHDISPLAYLENGTH: Long = 3000


    companion object {
        private const val MY_DATA = "data"
        fun newInstance(strData: String?) = SplashFragment().apply {
            arguments = bundleOf(
                MY_DATA to strData
            )
        }
    }


    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory //root factory that provides viewmodel for a fragment

    override fun getLayoutId(): Int = R.layout.fragment_splash          //assigning splash layout

    override fun getViewModel(): SplashFragmentViewModel =
        ViewModelProviders.of(this, mViewModelFactory).get(SplashFragmentViewModel::class.java)

    override fun getLifeCycleOwner(): LifecycleOwner = this


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        logger.printLog(TAG, "onViewCreated()")
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()

        hideKeyboard()
        requireActivity().onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().onBackPressed()
                }
            })
        Handler().postDelayed({
            proceedToRegister(HostFragment.newInstance(null))
        }, SPLASHDISPLAYLENGTH)
    }

    /*
    * setting up the toolbar/titlebar on top of the screen.
    * */
    private fun setUpToolbar() {
        logger.printLog(TAG, "setUpToolbar()")
        (activity as MainActivity).hideToolBar()
        (activity as MainActivity).hideBackButton()
    }

    private fun proceedToRegister(fragment: Fragment) {
        logger.printLog(TAG, "proceedToRegister")
        (activity as MainActivity).replaceFragment(fragment)
    }


}