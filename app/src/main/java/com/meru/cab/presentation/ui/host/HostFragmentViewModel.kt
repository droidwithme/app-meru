package com.meru.cab.presentation.ui.host

import com.meru.cab.presentation.base.BaseViewModel
import com.meru.cab.presentation.base.BaseViewState
import javax.inject.Inject

class HostFragmentViewModel @Inject constructor() : BaseViewModel<BaseViewState>()