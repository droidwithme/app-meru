

package com.meru.cab.presentation.di.module.potato

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.meru.cab.domain.usecase.PotatoRecipeUseCase
import com.meru.cab.presentation.commons.GridSpacingItemDecoration
import com.meru.cab.presentation.commons.ViewModelProviderFactory
import com.meru.cab.presentation.main.MainActivity
import com.meru.cab.presentation.ui.potato.PotatoRecipeAdapter
import com.meru.cab.presentation.ui.potato.PotatoRecipeFragment
import com.meru.cab.presentation.ui.potato.PotaotRecipeFragmentViewModel
import dagger.Module
import dagger.Provides


@Module
class PotatoFragmentModule {

    @Provides
    internal fun provideMainFragmentViewModel(potatoRecipeUseCase: PotatoRecipeUseCase): PotaotRecipeFragmentViewModel {
        return PotaotRecipeFragmentViewModel(potatoRecipeUseCase)
    }

    @Provides
    internal fun mainFragmentViewModelProvider(potaotRecipeFragmentViewModel: PotaotRecipeFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(potaotRecipeFragmentViewModel)
   }

    @Provides
    internal fun provideLinearLayoutManager(fragment: PotatoRecipeFragment): LinearLayoutManager {
        return LinearLayoutManager((fragment.activity as Context?)!!)
    }

    @Provides
    internal fun provideGridSpacingItemDecoration(): GridSpacingItemDecoration {
        return GridSpacingItemDecoration(2, 5, true)
    }

    @Provides
    internal fun provideMovieAdapter(context: MainActivity): PotatoRecipeAdapter {
        return PotatoRecipeAdapter(ArrayList(), context)
    }

}