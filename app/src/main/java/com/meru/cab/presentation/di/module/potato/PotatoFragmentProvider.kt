

package com.meru.cab.presentation.di.module.potato

import com.meru.cab.presentation.ui.potato.PotatoRecipeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class PotatoFragmentProvider {

    @ContributesAndroidInjector(modules =[(PotatoFragmentModule::class)])
    internal abstract fun provideMainFragmentFactory(): PotatoRecipeFragment

}