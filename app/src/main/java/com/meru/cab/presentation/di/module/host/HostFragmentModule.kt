package com.meru.cab.presentation.di.module.host

import androidx.lifecycle.ViewModelProvider
import com.meru.cab.presentation.commons.ViewModelProviderFactory
import com.meru.cab.presentation.ui.host.HostFragmentViewModel
import dagger.Module
import dagger.Provides

@Module
class HostFragmentModule {
    @Provides
    internal fun provideMainFragmentViewModel(): HostFragmentViewModel {
        return HostFragmentViewModel()
    }

    @Provides
    internal fun mainFragmentViewModelProvider(hostFragmentViewModel: HostFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(hostFragmentViewModel)
    }

}