package com.meru.cab.presentation.ui.fish

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.meru.cab.R
import com.meru.cab.domain.entities.Recipe
import com.meru.cab.presentation.base.BaseFragment
import com.meru.cab.presentation.base.BaseViewState
import com.meru.cab.presentation.commons.GridSpacingItemDecoration
import com.meru.cab.presentation.main.MainActivity
import kotlinx.android.synthetic.main.fragment_recipe.*
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class FishRecipeFragment : BaseFragment<FishRecipeFragmentViewModel>(),
    FishRecipeAdapter.RecipeCallbacks {

    companion object {
        val TAG = FishRecipeFragment::class.java.simpleName
        private const val MY_DATA = "data"
        fun newInstance(strData: String?) = FishRecipeFragment().apply {
            arguments = bundleOf(
                MY_DATA to strData
            )
        }
    }

    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory


    @Inject
    lateinit var mGridSpacingItemDecoration: GridSpacingItemDecoration

    @Inject
    lateinit var fishRecipeAdapter: FishRecipeAdapter


    override fun getLayoutId(): Int = R.layout.fragment_recipe
    override fun getViewModel(): FishRecipeFragmentViewModel =
        ViewModelProviders.of(this, mViewModelFactory)
            .get(FishRecipeFragmentViewModel::class.java)

    override fun getLifeCycleOwner(): LifecycleOwner = this


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e(TAG, "onViewCreated()")
        setUp()
        setUpUserRecyclerView()
        observeViewState()
        getViewModel().getFishRecipes()
    }

    private fun setUp() {
        Log.e(TAG, "setUp()")
        activity as MainActivity
    }

    override fun onResume() {
        super.onResume()

    }
    private fun setUpUserRecyclerView() {
        potatoRecyclerView.layoutManager = LinearLayoutManager(context)
        potatoRecyclerView.addItemDecoration(mGridSpacingItemDecoration)
        potatoRecyclerView.itemAnimator = DefaultItemAnimator()
        potatoRecyclerView.adapter = fishRecipeAdapter
        fishRecipeAdapter.setListener(this)
    }


    private fun observeViewState() {
        Log.e(TAG, "observeViewState()")
        getViewModel().uiState.observe(this, Observer {
            hideLoading()
            when (it) {
                is BaseViewState.messageText -> {
                    showMessage(it.text)
                }

                is BaseViewState.loading -> {
                    showLoading()
                }

                is BaseViewState.errorText -> {
                    onError(it.text)
                }

                is BaseViewState.hasData<*> -> {
                    if (it.data is ArrayList<*>) {
                        if (it.data.size > 0) {

                            fishRecipeAdapter.addItems(it.data as ArrayList<Recipe>)
                        } else {
                            onError("Recipe not found")
                        }
                    } else if (it.data is String) {
                        onSuccess(it.data)
                    }
                }
            }
        })
    }


    override fun onLikeDisLike(recipe: Recipe) {
        getViewModel().likeDislike(recipe)
    }

    override fun onDelete(recipe: Recipe) {
        getViewModel().deleteRecipe(recipe)
    }

    override fun onSave(recipe: Recipe) {
        onSuccess("Image/Media saved for offline use")
    }

}