package com.meru.cab.presentation.ui.chicken

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.himanshurawat.imageworker.Extension
import com.himanshurawat.imageworker.ImageWorker
import com.meru.cab.R
import com.meru.cab.domain.entities.Recipe
import com.meru.cab.presentation.commons.AppUtils
import kotlinx.android.synthetic.main.recipe_item.view.*
import java.lang.Exception
import javax.inject.Inject


class ChickenRecipeAdapter @Inject constructor(
    private val recipeList: MutableList<Recipe>,
    private val mContext: Context
) : RecyclerView.Adapter<ChickenRecipeAdapter.UserViewHolder>() {


    private val mLayoutInflater = LayoutInflater.from(mContext)
    private lateinit var callBackListener: RecipeCallbacks

    override fun getItemCount(): Int {
        return recipeList.size
    }

    fun setListener(mCallback: RecipeCallbacks) {
        callBackListener = mCallback
    }

    fun addItems(mList: ArrayList<Recipe>?) {
        if (mList != null) {
            clearItems()
            recipeList.addAll(mList)
            notifyDataSetChanged()
        }
    }


    private fun clearItems() {
        recipeList.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = mLayoutInflater.inflate(R.layout.recipe_item, parent, false)
        return UserViewHolder(view)
    }

    // stores and recycles views as they are scrolled off screen
    inner class UserViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewTitle = itemView.textViewTitle as TextView
        var textViewPublisher = itemView.textViewPublisher as TextView
        var imageView = itemView.imageViewRecipe as ImageView
        var imageSave = itemView.imageViewSave as ImageView
        var imageDelete = itemView.imageViewDelete as ImageView
        var checkBox = itemView.checkboxLike as CheckBox


    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val recipe = recipeList[position]
        holder.textViewTitle.text = recipe.title
        holder.textViewPublisher.text = "Recipe By: ${recipe.publisher}"

        if (AppUtils.isNetworkConnected(mContext)) {
            Glide.with(mContext)
                .load(recipe.image_url)
                .into(holder.imageView)
            holder.imageView.isDrawingCacheEnabled = true
        } else {
            val bitmap: Bitmap? =
                ImageWorker.from(mContext).directory("Meru").setFileName(recipe._id)
                    .withExtension(Extension.PNG).load()
            if (bitmap != null) {
                Log.e("Adapter", "Image found")
            }
            Glide.with(mContext)
                .load(bitmap)
                .into(holder.imageView)
        }

        holder.checkBox.isChecked = recipe.isLiked

        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            recipe.isLiked = isChecked
            callBackListener.onLikeDisLike(recipe)

        }
        holder.imageDelete.setOnClickListener {
            recipeList.remove(recipe)
            callBackListener.onDelete(recipe)
        }
        holder.imageSave.setOnClickListener {
            try {
                ImageWorker.to(mContext).directory("Meru").setFileName(recipe._id)
                    .withExtension(Extension.PNG).save(holder.imageView.getDrawingCache(), 100)
                callBackListener.onSave(recipe)
            } catch (e: Exception) {
                Toast.makeText(mContext, e.message, Toast.LENGTH_SHORT).show()
            }

        }
    }

    interface RecipeCallbacks {
        fun onLikeDisLike(recipe: Recipe)
        fun onDelete(recipe: Recipe)
        fun onSave(recipe: Recipe)
    }

}