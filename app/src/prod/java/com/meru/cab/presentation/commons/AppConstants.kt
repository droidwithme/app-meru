

package com.meru.cab.presentation.commons


/*
* Required global constants
* */
class AppConstants {

    companion object {
       const val BASE_URL: String = ("http://recipesapi.herokuapp.com/api/")
    }
}